class Temperature
  def initialize(temperature)
    @temperature = temperature

  end

  def temperature
    @temperature
  end

  def in_fahrenheit
    if self.temperature.key?(:f)
      self.temperature[:f].to_f
    else
      (self.temperature[:c].to_f * 9.0 / 5.0) + 32.0
    end
  end

  def in_celsius
    if self.temperature.key?(:c)
      self.temperature[:c].to_f
    else
      (self.temperature[:f].to_f - 32.0) * 5.0 / 9.0
    end
  end

  def self.from_celsius(celsius)
    return Temperature.new({:c => celsius})
  end

  def self.from_fahrenheit(fahrenheit)
    return Temperature.new({:f => fahrenheit})
  end
end
  class Celsius < Temperature
    def initialize(temperature)
      @temperature = {:c => temperature}
    end
  end

  class Fahrenheit < Temperature
    def initialize(temperature)
      @temperature = {:f => temperature}
    end
  end
