class Timer
  def initialize(seconds = 0)
    @seconds = 0
  end

  def seconds
    @seconds
  end

  def seconds=(seconds)
    @seconds = seconds
  end

  def time_string
    p self.seconds
    if self.seconds.nil?
      total_seconds = 0
    else
      total_seconds = self.seconds
    end
    total_minutes = 0
    total_hours = 0

    while total_seconds >= 3600
      total_hours += 1
      total_seconds -= 3600
    end

    while total_seconds >= 60
      total_minutes += 1
      total_seconds -= 60
    end
    number_array = [total_hours, total_minutes, total_seconds]
    string_array = number_array.map do |number|
      if number < 10
        "0#{number}"
      else "#{number}"
      end
    end
    string_array.join(":")
  end
end
