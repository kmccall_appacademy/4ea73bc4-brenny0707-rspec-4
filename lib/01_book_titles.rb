class Book

  def title
    @title
  end

  def title=(title)
    @title = title
    reword = @title.split(" ").map do |word|
      exceptions = ["a", "the", "an", "and", "but", "of", "in", "on", "from", "off"]
      if exceptions.include?(word)
        word
      else
        word.capitalize
      end
    end
    reword.first.capitalize!
    @title = reword.join(" ").strip
  end


end
