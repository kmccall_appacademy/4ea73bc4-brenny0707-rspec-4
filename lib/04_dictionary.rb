class Dictionary
  def initialize
    @dictionary = {}
  end

  def entries
   @dictionary
  end

  def add(word)
    if word.class == Hash
      word.each do |key, value|
        @dictionary[key] = value
      end
    else
      @dictionary[word] = nil
    end
  end

  def keywords
    @dictionary.keys.sort_by { |k, v| k }
  end

  def include?(word)
    keywords.include?(word)
  end

  def find(keyword)
    @dictionary.select { |k, v| k.include?(keyword) }
  end

  def printable
    string = ""
    keywords.sort.each do |k|
      string << "[#{k}] \"#{@dictionary[k]}\"\n"
      p string
    end
    string.chomp
  end
end
